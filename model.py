from kafka import KafkaProducer, KafkaConsumer
import random


model_producer = KafkaProducer()
model_consumer = KafkaConsumer()


def make_prediction(topic, rec):
    rec['score'] = random.randint(0, 100)
    model_producer.produce('model_results', [rec])


if __name__ == '__main__':
    model_consumer.consume(['derived_fields'], process_func=make_prediction)