from kafka import KafkaProducer
import random


etl_producer = KafkaProducer()

def derive_fields(data):
    for rec in data:
        rec['recommits'] = rec['stage']  + random.randint(0, 3)
        etl_producer.produce('derived_fields', [rec])

if __name__ == '__main__':
    data = [{'id': random.randint(500, 600), 
             'type': random.choice(['New', 'Renewal', 'Services']), 
             'stage': random.choice([1, 2, 3])} for x in range(10)]

    etl_producer.produce('raw_fields', data)
    derive_fields(data)

